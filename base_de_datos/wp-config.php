<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', 'C:\Users\DAW-09\Desktop\xampp\htdocs\wordpress\wp-content\plugins\wp-super-cache/' );
define( 'DB_NAME', 'base_datos_sistemas_informaticos' );

/** Database username */
define( 'DB_USER', 'usuario' );

/** Database password */
define( 'DB_PASSWORD', 'usuario' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '@!k+lIXZvD,xQj)H`,5=kp_CeB?n?I~t6?;i;NPk?eZptzOVgYZGXrv[N/3b3o(`' );
define( 'SECURE_AUTH_KEY',  'z42 U&(p3wY,xbEphTot7O:nv-q~U;B8Q!(+k@@tqF>A91Y:@pDUmrPWR,,yIdVz' );
define( 'LOGGED_IN_KEY',    'P0^hWXr,Se~O0Tp2+dDK#)Mq.4 ` is;}V{Ez1#N>{kiAw`+wN1 b.at;N>j|?$~' );
define( 'NONCE_KEY',        'DKgZkk3B$#K^ &2~4k@})#r*M<.JhgT,ygg|]<e_p/<T( ge gv|Z}u0(r4Q7aQx' );
define( 'AUTH_SALT',        '7Xo[ln@VAU1^,b3svBb3HY{|@{iT_]GOtp6f_Ey|]bsX$@2g@,hbOR*T$tVH6=BY' );
define( 'SECURE_AUTH_SALT', 'r|7|r+31?c93fYxG5A) HHl..#UuSu>JlM<v~FK;Ph7(QvY+lN??sIxQ7#uRM,O[' );
define( 'LOGGED_IN_SALT',   'h[uh{-gTXR ?xIPRB_Su6mMd;mp?3.Fy=McdK<B:1@[M)_o;d%e-sB#UC?4+Ej$W' );
define( 'NONCE_SALT',       '0adH~tqGT 3,[1kb3Q5{DS0Ag%a_2{^yT|3fG8Dg{#`c>;u-i-PoAF+S~v__Ptc1' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
